//
//  ViewController.swift
//  RoadCode
//
//  Created by rockymay on 2017/3/19.
//  Copyright © 2017年 com.rockymay. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.loadRoadCodeData(rawFile: "roadcode", ext: "csv")
        
        showQuestionTitle(index: questionNo)
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //loadRoadCodeData(rawFile: "roadcode", ext: "csv")
    
    func loadRoadCodeData(rawFile: String, ext: String){
        let file = Bundle.main.path(forResource: rawFile, ofType: ext)
        let url = NSURL(fileURLWithPath: file!)
        let wholeFile = try? String(contentsOf: url as URL)
        let questionArray = (wholeFile?.components(separatedBy: CharacterSet.newlines))! as [String]
        
        for item in questionArray {
            let values = item.components(separatedBy: "#") as [String]
            if values.count > 3 {
                questionDobbleArray.append(values)
            }
        }
        print("You have inserted " + String(questionDobbleArray.count - 1) + " questions into database")
        
    }
    
    var questionDobbleArray: Array<Array<String>> = Array<Array<String>>()
    var questionNo = 1
    var isThisSelected = false
    

    
    @IBOutlet weak var optionAnswerA: UILabel!
    
    @IBOutlet weak var optionAnswerB: UILabel!
    
    @IBOutlet weak var optionAnswerC: UILabel!
    
    @IBOutlet weak var optionAnswerD: UILabel!
    
    @IBOutlet weak var optionAnswerE: UILabel!
    
    @IBOutlet weak var showCorrectAnswer: UILabel!
    
    
    
    @IBOutlet weak var topMessage: UILabel!
    @IBOutlet weak var questionTitle: UILabel!
    func showQuestionTitle(index: Int){
        questionTitle.text = questionDobbleArray[index][0]
        topMessage.text = "Question No " + String(questionNo) + "  Total " + String(questionDobbleArray.count - 1)
        topMessage.textColor = UIColor.blue
        optionAnswerA.text = questionDobbleArray[index][2]
        optionAnswerB.text = questionDobbleArray[index][3]
        optionAnswerC.text = questionDobbleArray[index][4]
        if questionDobbleArray[index].count > 5 {
            optionAnswerD.text = questionDobbleArray[index][5]
            if questionDobbleArray[index].count > 6 {
                optionAnswerE.text = questionDobbleArray[index][6]
            }
        }
        
        
        
        
    }
    
    @IBAction func questionSelection(_ sender: UIButton) {
        sender.showsTouchWhenHighlighted = true
        resetQuestionDetail()
        switch sender.tag {
            
        case 0:
            if questionNo <= 1{
                
            }
            else {
                questionNo -= 1
            }
            
        case 1:
            if questionNo >= questionDobbleArray.count-1 {
                break
            }
            else {
                questionNo += 1
            }
        case 2:
            showCorrectAnswer.text = questionDobbleArray[questionNo][1]
        default:
            break
        }
        //showQuestionDetail(index: questionNo)
        showQuestionTitle(index: questionNo)
    }
    
    func resetQuestionDetail(){
        questionTitle.text = ""
        topMessage.text = ""
        optionAnswerA.text = ""
        optionAnswerB.text = ""
        optionAnswerC.text = ""
        optionAnswerD.text = ""
        optionAnswerE.text = ""
        showCorrectAnswer.text = ""

        
    }
}



